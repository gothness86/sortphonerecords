﻿$TargetFolder = "D:\Record"
$Extension = "*.wav"

$Files = Get-Childitem "$TargetFolder\*" -Include $Extension

foreach ($File in $Files) {

	if ($File -ne $NULL)
	{
		Write-Host "Found $File" -ForegroundColor "Yellow"
		$Target = $TargetFolder + "\" + $File.LastWriteTime.ToString("yyyy.M")
		if ( (Test-Path $Target ) -eq 0) { New-Item -ItemType Directory -Path $Target | out-null }
		Move-Item $File $Target
	}
	else { Write-Host "No more files to move!" -foregroundcolor "Red" }
}	
